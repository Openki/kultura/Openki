import { Template as TemplateAny, TemplateStaticTyped } from 'meteor/templating';
import moment from 'moment';

import { EventModel } from '/imports/api/events/events';

import '/imports/ui/components/groups/list';
import '/imports/ui/components/venues/link';

import './template.html';
import './styles.scss';

const Template = TemplateAny as TemplateStaticTyped<
	'eventCompact',
	{
		event: EventModel;
		withDate?: boolean;
		withImage?: boolean;
		openInNewTab?: boolean;
	},
	{
		isFullyBooked: () => boolean;
	}
>;

const template = Template.eventCompact;

template.onCreated(function () {
	const instance = this;

	instance.isFullyBooked = () => {
		const { event } = instance.data;
		if (!event.maxParticipants) {
			return false;
		}
		return (event.participants?.length || 0) >= event.maxParticipants;
	};
});

template.helpers({
	userAttendsEvent() {
		const { participants } = Template.instance().data.event;
		const userId = Meteor.userId();
		if (!userId) {
			return false;
		}
		return participants?.includes(userId);
	},

	eventCompactClasses() {
		const instance = Template.instance();
		const { event, withDate } = instance.data;

		const classes = [];
		if (withDate) {
			classes.push('has-date');
		}
		if (moment().isAfter(event.end) || instance.isFullyBooked()) {
			classes.push('is-past');
		}

		return classes.join(' ');
	},

	showImage() {
		const { event, withImage } = Template.instance().data;

		if (!withImage) {
			return false;
		}

		const src = event?.publicImageUrl();
		if (!src) {
			return false;
		}

		return true;
	},

	bodyStyle() {
		const { event, withImage } = Template.instance().data;

		let colors = [
			'#1D5A70',
			'#176A7F',
			'#0D858E',
			'#1FB49F',
			'#00A59C',
			//'#62BF93',
			'#D18B3E',
			'#CE732E',
			'#C9491C',
			'#B62721',
			'#A8172B',
			'#8E113B'
		]

		if (!withImage) {
			let randomColor = colors[Math.random() * colors.length | 0]
			return {
				style: `background-color: ${randomColor}`
			};
		}

		const src = event?.publicImageUrl();
		if (!src) {
			let randomColor = colors[Math.random() * colors.length | 0]
			return {
				style: `background-color: ${randomColor}`
			};
		}

		return {
			style: `
	background-image: url('${src}');
	background-position: center;
	background-size: cover;`,
		};
	},
});

template.events({
	'mouseover .js-venue-link, mouseout .js-venue-link'(_event, instance) {
		instance.$('.event-compact').toggleClass('elevate-child');
	},
});
