import { Session } from 'meteor/session';
import { Router } from 'meteor/iron:router';
import { Template as TemplateAny, TemplateStaticTyped } from 'meteor/templating';
import $ from 'jquery';

import { Regions } from '/imports/api/regions/regions';

import * as RegionSelection from '/imports/utils/region-selection';
import { Introduction } from '/imports/ui/lib/introduction';
import { ScssVars } from '/imports/ui/lib/scss-vars';
import * as Viewport from '/imports/ui/lib/viewport';
import { routerAutoscroll } from '/imports/ui/lib/router-autoscroll';
import * as UserPrivilegeUtils from '/imports/utils/user-privilege-utils';

import '/imports/ui/components/account-tasks';
import '/imports/ui/components/alerts';
import '/imports/ui/components/email-request';
import '/imports/ui/components/email-validation';
import '/imports/ui/components/featured-group';
import '/imports/ui/components/footer';
import '/imports/ui/components/introduction/introduction';
import '/imports/ui/components/kiosk-link';
import '/imports/ui/components/navbar';
import '/imports/ui/components/regions/splash';

import './template.html';

const Template = TemplateAny as TemplateStaticTyped<'layout'>;

const template = Template.layout;

function getPos(speed:number, offset_percent:number) : number {
	let scroll = $(document).scrollTop() ?? 0;
	let safeZone = 400;
	let scrollHeight = window.innerHeight + 2 * safeZone;
	let offset = offset_percent * window.innerHeight;
	let pos = (scroll + offset) * speed;
	return (pos % scrollHeight) - safeZone;
}

template.helpers({
	showRegionSplash() {
		const { route } = Router.current();
		if (!route) {
			return false;
		}

		return (
			RegionSelection.regionDependentRoutes.includes(route.getName()) &&
			Session.equals('showRegionSplash', true)
		);
	},

	hasPricePolicy() {
		return !Regions.currentRegion()?.isPrivate();
	},

	isAdminPage() {
		return Router.current().url.includes('admin');
	},

	isAdmin() {
		return UserPrivilegeUtils.privilegedTo('admin');
	},

	isNotAdminPanel() {
		const { route } = Router.current();
		return !(route?.getName() === 'adminPanel');
	},
});

template.events({
	// Clicks on the logo toggle the intro blurb, but only when already on home
	'click .js-toggle-introduction'() {
		const { route } = Router.current();
		if (route?.getName() === 'home') {
			Introduction.showIntro();
		}
	},
});

template.onRendered(() => {
	Viewport.update();
	$(window).on('resize', () => {
		Viewport.update();
	});
	Session.set('isRetina', window.devicePixelRatio === 2);

	$(".floating-element#l1").css('bottom', getPos(0.75, 0.7) + 'px');
	$(".floating-element#l2").css('bottom', getPos(0.85, 0.8) + 'px');
	$(".floating-element#l3").css('bottom', getPos(0.45, 0.1) + 'px');
	$(".floating-element#l4").css('bottom', getPos(0.6, 0.5) + 'px');
	$(".floating-element#l5").css('bottom', getPos(0.35, 0.5) + 'px');
	$(".floating-element#l6").css('bottom', getPos(0.8, 0.1) + 'px');

	$(".floating-element#r1").css('bottom', getPos(0.75, 0.3) + 'px');
	$(".floating-element#r2").css('bottom', getPos(0.85, 0.3) + 'px');
	$(".floating-element#r3").css('bottom', getPos(0.45, 0.1) + 'px');
	$(".floating-element#r4").css('bottom', getPos(0.6, 0.7) + 'px');
	$(".floating-element#r5").css('bottom', getPos(0.35, 0.5) + 'px');

	$(document).on('scroll', () => {
		$(".floating-element#l1").css('bottom', getPos(0.75, 0.7) + 'px');
		$(".floating-element#l2").css('bottom', getPos(0.85, 0.8) + 'px');
		$(".floating-element#l3").css('bottom', getPos(0.45, 0.1) + 'px');
		$(".floating-element#l4").css('bottom', getPos(0.6, 0.5) + 'px');
		$(".floating-element#l5").css('bottom', getPos(0.35, 0.5) + 'px');
		$(".floating-element#l6").css('bottom', getPos(0.8, 0.1) + 'px');

		$(".floating-element#r1").css('bottom', getPos(0.75, 0.3) + 'px');
		$(".floating-element#r2").css('bottom', getPos(0.85, 0.3) + 'px');
		$(".floating-element#r3").css('bottom', getPos(0.45, 0.1) + 'px');
		$(".floating-element#r4").css('bottom', getPos(0.6, 0.7) + 'px');
		$(".floating-element#r5").css('bottom', getPos(0.35, 0.5) + 'px');

		$('.decoration.scroll.one').css('top', (-($(document).scrollTop() ?? 0) * 0.25) + 'px');
		$('.decoration.scroll.two').css('top', (-($(document).scrollTop() ?? 0) * 0.65) + 'px');
	  });
});

template.events({
	/* Workaround to prevent iron-router from messing with server-side downloads
	 *
	 * Class 'js-download' must be added to those links.
	 */
	'click .js-download'(event) {
		event.stopPropagation();
	},
});

routerAutoscroll.marginTop = ScssVars.navbarHeight;
